#include "Corrections/BTV/interface/btag_SFinterface.h"

// Constructor
btag_SFinterface::btag_SFinterface (int year, std::string filename):
  corr_incl(filename, "deepJet_incl"),
  // corr_mujets(filename, "deepJet_mujets"),
  corr_comb(filename, "deepJet_comb"),
  corr_shape(filename, "deepJet_shape")
{
  year_ = year;
  if (year_ == 2018) {
    deepjet_discr = deepjet_discr_UL2018;
  }
}


std::vector<double> btag_SFinterface::get_btag_sf(
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_jetId, iRVec Jet_puId, iRVec Jet_hadronFlavour, fRVec Jet_btagDeepFlavB,
    fRVec lepton_pt, fRVec lepton_eta, fRVec lepton_phi, fRVec lepton_mass,
    std::string incl_uncertainty, std::string comb_uncertainty, std::string shape_uncertainty) {

  std::vector<double> sf(4, 1.);

  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if (Jet_jetId[ijet] < 2)
      continue;
    if (Jet_pt[ijet] < 20)
      continue;
    if (Jet_puId[ijet] < 1 && Jet_pt[ijet] < 50)
      continue;
    if (fabs(Jet_eta[ijet]) >= 2.5)  // in legacy, 2.4
      continue;

    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);

    bool lepton_match = false;
    for (size_t ilep = 0; ilep < lepton_pt.size(); ilep++) {
      auto lep_tlv = TLorentzVector();
      lep_tlv.SetPtEtaPhiM(lepton_pt[ilep], lepton_eta[ilep], lepton_phi[ilep], lepton_mass[ilep]);
      if (jet_tlv.DeltaR(lep_tlv) < 0.5) {
        lepton_match = true;
        break;
      }
    }
    if (lepton_match) continue;
    // std::cout << deepjet_discr.size() << std::endl;
    for (size_t iwp = 0; iwp < wps.size(); iwp++) {
      if (Jet_btagDeepFlavB[ijet] < deepjet_discr[iwp])
        continue;
      if (Jet_hadronFlavour[ijet] == 0) {
        sf[iwp] *= corr_incl.eval({incl_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      } else if (Jet_hadronFlavour[ijet] == 4) {
        sf[iwp] *= corr_comb.eval({comb_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      } else if (Jet_hadronFlavour[ijet] == 5) {
        sf[iwp] *= corr_comb.eval({comb_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      }
    }
    auto val = corr_shape.eval({shape_uncertainty,
      Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet], Jet_btagDeepFlavB[ijet]});
    // std::cout << "Jet pt " << Jet_pt[ijet] << ", Jet eta " << Jet_eta[ijet] << ", value " <<  val << std::endl;
    sf[3] *= val;
  }  // end loop over jets

  return sf;

}


// Destructor
btag_SFinterface::~btag_SFinterface() {}
